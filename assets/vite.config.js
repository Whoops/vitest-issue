import { svelte } from "@sveltejs/vite-plugin-svelte";
//Painful polyfills for PouchDB.
import nodePolyfills from "rollup-plugin-node-polyfills";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig((ops) => {
  const isDev = ops.command !== "build";
  const isGitlab = process.env.GITLAB_CI
  if (isDev && !isGitlab) {
    // Terminate the watcher when Phoenix quits
    process.stdin.on("close", () => {
      process.exit(0);
    });

    process.stdin.resume();
  }

  return {
    plugins: [svelte(), tsconfigPaths()],
    resolve: {
      alias: {
        events: "rollup-plugin-node-polyfills/polyfills/events",
      },
    },
  };
});
